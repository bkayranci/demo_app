from fastapi import FastAPI

app = FastAPI()

@app.get("/")
def read_root():
    msg = {"Hello": "World"}
    print(msg)
    return msg

